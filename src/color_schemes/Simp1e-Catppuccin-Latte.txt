name:Simp1e Catppuccin Latte

# Shadow: Text
shadow:4c4f69
shadow_opacity:0.35

# Border: Text
cursor_border:4c4f69

# Normal background: Base
default_cursor_bg:eff1f5
hand_bg:eff1f5

# Sky
question_mark_bg:04a5e5
# Colorful backgrounds' foreground: Base
question_mark_fg:eff1f5

# Green
plus_bg:40a02b
plus_fg:eff1f5

# Mauve
link_bg:8839ef
link_fg:eff1f5

# Yellow
move_bg:df8e1d
move_fg:eff1f5

# Blue
context_menu_bg:1e66f5
context_menu_fg:eff1f5

forbidden_bg:eff1f5
# Red
forbidden_fg:d20f39

magnifier_bg:eff1f5
magnifier_fg:4c4f69

skull_bg:eff1f5
skull_eye:4c4f69

spinner_bg:eff1f5
spinner_fg1:4c4f69
spinner_fg2:4c4f69
