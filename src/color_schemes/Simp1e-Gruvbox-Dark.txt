name:Simp1e Gruvbox Dark

shadow:282828
shadow_opacity:0.35

cursor_border:ebdbb2

default_cursor_bg:282828
hand_bg:282828

question_mark_bg:689d6a
question_mark_fg:ebdbb2

plus_bg:98971a
plus_fg:ebdbb2

link_bg:b16286
link_fg:ebdbb2

move_bg:d79921
move_fg:ebdbb2

context_menu_bg:458588
context_menu_fg:ebdbb2

forbidden_bg:ebdbb2
forbidden_fg:cc241d

magnifier_bg:282828
magnifier_fg:ebdbb2

skull_bg:282828
skull_eye:ebdbb2

spinner_bg:282828
spinner_fg1:ebdbb2
spinner_fg2:ebdbb2
