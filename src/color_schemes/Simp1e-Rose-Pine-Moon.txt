name:Simp1e Rose Pine Moon

# Shadow: Base
shadow:232136
shadow_opacity:0.35

# Border: Text
cursor_border:e0def4

# Normal background: Base
default_cursor_bg:232136
hand_bg:232136

# Foam
question_mark_bg:9ccfd8
# Colorful backgrounds' foreground: Surface
question_mark_fg:2a273f

# Rose
plus_bg:ea9a97
# Surface
plus_fg:2a273f

# Iris
link_bg:c4a7e7
# Surface
link_fg:2a273f

# Gold
move_bg:f6c177
# Surface
move_fg:2a273f

# Pine
context_menu_bg:3e8fb0
# Surface
context_menu_fg:2a273f

# Love
forbidden_fg:eb6f92
# Surface
forbidden_bg:2a273f

# Base
magnifier_bg:232136
# Text
magnifier_fg:e0def4

# Base
skull_bg:232136
# Text
skull_eye:e0def4

spinner_bg:232136:
# Text
spinner_fg1:e0def4
spinner_fg2:e0def4
