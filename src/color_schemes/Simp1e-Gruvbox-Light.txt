name:Simp1e Gruvbox Light

shadow:282828
shadow_opacity:0.35

cursor_border:3c3836

default_cursor_bg:fbf1c7
hand_bg:fbf1c7

question_mark_bg:689d6a
question_mark_fg:3c3836

plus_bg:98971a
plus_fg:3c3836

link_bg:b16286
link_fg:3c3836

move_bg:d79921
move_fg:3c3836

context_menu_bg:458588
context_menu_fg:3c3836

forbidden_bg:fbf1c7
forbidden_fg:cc241d

magnifier_bg:fbf1c7
magnifier_fg:3c3836

skull_bg:fbf1c7
skull_eye:3c3836

spinner_bg:fbf1c7
spinner_fg1:3c3836
spinner_fg2:3c3836
